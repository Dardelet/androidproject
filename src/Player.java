import java.util.ArrayList;


public class Player 
{
	private String playerName; 
	private int posX; // Position GPS du joueur
	private int posY;
	private int level; // son niveau
	private int xpCount; // son nombre d'xp (il peut être niveau 10 à la moitié du niveau 11)
	private ArrayList<Tower> towerList; // Liste des tours qu'il possède
	private int goldAmount; // Quantité de ressource "gold" que possède le joueur
	private int lvlABloc; // Niveau d'amélioration du bloc d'attaque
	private int lvlDBloc; // 					   du bloc défense
	private int lvlRBloc; //du bloc ressource

	public Player(String playerName)
	{
		this.playerName = playerName;
		posX = 0;
		posY = 0;
		level = 1;
		xpCount = 0;
		towerList = null;
		goldAmount = 100 ;
		lvlABloc = 1;
		lvlDBloc = 1;
		lvlRBloc = 1;
	}

	public int calculateMaxHeight() 
	{
		return 1;
	}
	
	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

}
