import java.util.ArrayList;

public class Tower 
{
	private Bloc[] tower;
	int posX;
	int posY;
	int maxHeight;
	private Player player;
	
	public Tower(Player player, ArrayList<Bloc> listBloc)
		throws HeightException
	{
		int n = listBloc.size();
		maxHeight = player.calculateMaxHeight();
		
		if (n > maxHeight)
			throw new HeightException();
		 
		this.player = player;
		this.posX = player.getPosX;
		this.posY = player.getPosY;
		tower = new Bloc[n];
		int i = 0;
		for (Bloc bloc : listBloc)
		{
			tower[i] = bloc;
			i++;
		}
	}
}