import java.util.ArrayList;


public class Main 
{

	public static void main(String[] args) 
	
	{
		Player player = new Player("Guillaume");
		ArrayList<Bloc> listBloc = new ArrayList<Bloc>();
		listBloc.add(new ABloc(player));
		listBloc.add(new DBloc(player));
		
		try 
		{
			Tower tower = new Tower(player, listBloc);
		} 
		catch (HeightException e) {e.printStackTrace();}
	}

}
